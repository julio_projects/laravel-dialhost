<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Usuario;

class UsuariosController extends Controller
{
    public function index()
    {
      $usuarios = Usuario::all();

      if (!$usuarios) {
        throw new HttpException(400, "Invalid data");
      }

      //return view('usuarios.index', compact('usuarios'));
      return response()->json([
            $usuarios,
        ], 200);

    }

    public function show($id)
    {
      if (!$id) {
         throw new HttpException(400, "Invalid id");
      }

      $usuario = Usuario::find($id);

      //return view('usuarios.show', compact('usuario'));
      return response()->json([
            $usuario,
        ], 200);
    }

    public function store(Request $request)
    {
      $usuario = new Usuario;
      $usuario->nome = $request->input('nome');
      $usuario->email = $request->input('email');
      $usuario->telefone = $request->input('telefone');
      $usuario->data_nascimento = $request->input('data_nascimento');
      if ($usuario->save()) {
          return $usuario;
      }

      throw new HttpException(400, "Invalid data");
    }

    public function update(Request $request, $id)
    {
        if (!$id) {
            throw new HttpException(400, "Invalid id");
        }

        $usuario = Usuario::find($id);
        $usuario->nome = $request->input('nome');
        $usuario->email = $request->input('email');
        $usuario->telefone = $request->input('telefone');
        $usuario->data_nascimento = $request->input('data_nascimento');
        if ($usuario->save()) {
          return $usuario;
        }

        throw new HttpException(400, "Invalid data");
    }
    public function destroy($id)
    {
        if (!$id) {
            throw new HttpException(400, "Invalid id");
        }
        $usuario = Usuario::find($id);
        $usuario->delete();

        return response()->json([
            'message' => 'Usuário deletado',
        ], 200);
    }
}
