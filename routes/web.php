<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Usuario;

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => 'cors'], function(){
  Route::get('/usuarios', 'UsuariosController@index');
  Route::get('/usuarios/{usuario}','UsuariosController@show');
  Route::post('/usuarios', 'UsuariosController@store');
  Route::put('/usuarios/{id}', 'UsuariosController@update');
  Route::delete('/usuarios/{id}', 'UsuariosController@destroy');
});
