<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Usuários</title>
</head>
<body>
  <h1>Usuários</h1>
  <ul>
    @foreach ($usuarios as $usuario)
      <li><a href="/usuarios/{{ $usuario->id }}">{{ $usuario->nome }}</a></li>
    @endforeach
  </ul>

</body>
</html>
